@echo off

:start
set masterfolder=%cd%
cd ".\ArknightsAutoHelper"
call mode con cp select=936 > NUL
call venv\Scripts\activate.bat
cls


:run
cls
set times=""
set /p times="Please enter the number of cycles (Default = Unlimited) : "
if %times%=="" (call python akhelper.py quick -r) else (call python akhelper.py quick +rR %times%)

:askContinue
set /p continue=Rerun the script ? (Y/N):|| set "continue=y"
if /i %continue%==n goto end
if /i %continue%==y goto run

:end
exit