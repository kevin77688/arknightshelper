@echo off


:start
set masterfolder=%cd%
cd ".\ArknightsAutoHelper"
call mode con cp select=936 > nul
call venv\Scripts\activate.bat
cls

:run
cls
call python akhelper.py recruit
call .\ADB\win32\adb shell wm size reset

:askContinue
set /p continue=Rerun the script ? (Y/N): || set "continue=y"
if /i %continue%==y goto run
if /i %continue%==n goto end

:end
exit