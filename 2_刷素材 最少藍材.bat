@echo off

:start
set masterfolder=%cd%
cd ".\ArknightsAutoHelper"
call mode con cp select=936 > NUL
call venv\Scripts\activate.bat
cls

:run
cls
call python grass_on_aog.py

:end
pause