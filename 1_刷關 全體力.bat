@echo off

:start
set masterfolder=%cd%
cd ".\ArknightsAutoHelper"
call mode con cp select=936 > NUL
call venv\Scripts\activate.bat
cls


:run
cls
call python akhelper.py quick -r

:askContinue
set /p continue=Rerun the script ? (Y/N):
if /i %continue%==n goto end
if /i %continue%==y goto run

:end
exit