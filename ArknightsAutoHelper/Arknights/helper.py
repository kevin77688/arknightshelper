import os
import json
import re
import time
import logging
from typing import Callable
from dataclasses import dataclass
from random import randint, uniform, gauss
from time import sleep, monotonic
from fractions import Fraction

import coloredlogs
import numpy as np

import config
import imgreco.common
import imgreco.main
import imgreco.task
import imgreco.map
import imgreco.imgops
import penguin_stats.reporter
from connector import auto_connect
from connector.ADBConnector import ADBConnector, ensure_adb_alive
from . import stage_path
from .frontend import DummyFrontend
from Arknights.click_location import *
from Arknights.flags import *
from util.excutil import guard

from Arknights import frontend

logger = logging.getLogger('helper')
coloredlogs.install(
    fmt=' Ξ %(message)s',
    #fmt=' %(asctime)s ! %(funcName)s @ %(filename)s:%(lineno)d ! %(levelname)s # %(message)s',
    datefmt='%H:%M:%S',
    level_styles={'warning': {'color': 'green'}, 'error': {'color': 'red'}},
    level='INFO')


def item_name_guard(item):
    return str(item) if item is not None else '<無法識別的物品>'


def item_qty_guard(qty):
    return str(qty) if qty is not None else '?'


def format_recoresult(recoresult):
    result = None
    with guard(logger):
        result = '[%s] %s' % (recoresult['operation'],
            '; '.join('%s: %s' % (grpname, ', '.join('%sx%s' % (item_name_guard(itemtup[0]), item_qty_guard(itemtup[1]))
            for itemtup in grpcont))
            for grpname, grpcont in recoresult['items']))
    if result is None:
        result = '<發生錯誤>'
    return result


class ArknightsHelper(object):
    def __init__(self, adb_host=None, device_connector=None, frontend=None):  # 當前綁定到的設備
        self.adb = None
        if adb_host is not None or device_connector is not None:
            self.connect_device(device_connector, adb_serial=adb_host)
        if frontend is None:
            frontend = DummyFrontend()
            if self.adb is None:
                self.connect_device(auto_connect())
        self.frontend = frontend
        self.frontend.attach(self)
        self.operation_time = []
        if DEBUG_LEVEL >= 1:
            self.__print_info()
        self.refill_with_item = config.get('behavior/refill_ap_with_item', False)
        self.refill_with_originium = config.get('behavior/refill_ap_with_originium', False)
        self.use_refill = self.refill_with_item or self.refill_with_originium
        self.loots = {}
        self.use_penguin_report = config.get('reporting/enabled', False)
        if self.use_penguin_report:
            self.penguin_reporter = penguin_stats.reporter.PenguinStatsReporter()
        self.refill_count = 0
        self.max_refill_count = None

        logger.debug("成功初始化模塊")

    def ensure_device_connection(self):
        if self.adb is None:
            raise RuntimeError('not connected to device')

    def connect_device(self, connector=None, *, adb_serial=None):
        if connector is not None:
            self.adb = connector
        elif adb_serial is not None:
            self.adb = ADBConnector(adb_serial)
        else:
            self.adb = None
            return
        self.viewport = self.adb.screen_size
        if self.viewport[1] < 720 or Fraction(self.viewport[0], self.viewport[1]) < Fraction(16, 9):
            title = '設備當前解析度（%dx%d）不符合要求' % (self.viewport[0], self.viewport[1])
            body = '需要寬高比等於或大於 16∶9，且渲染高度不小於 720。'
            details = None
            if Fraction(self.viewport[1], self.viewport[0]) >= Fraction(16, 9):
                body = '屏幕截圖可能需要旋轉，請嘗試在 device-config 中指定旋轉角度。'
                img = self.adb.screenshot()
                imgfile = os.path.join(config.SCREEN_SHOOT_SAVE_PATH, 'orientation-diagnose-%s.png' % time.strftime("%Y%m%d-%H%M%S"))
                img.save(imgfile)
                import json
                details = '參考 %s 以更正 device-config.json[%s]["screenshot_rotate"]' % (imgfile, json.dumps(self.adb.config_key))
            for msg in [title, body, details]:
                if msg is not None:
                    logger.warn(msg)
            frontend.alert(title, body, 'warn', details)

    def __print_info(self):
        logger.info('當前系統信息:')
        logger.info('解析度:\t%dx%d', *self.viewport)
        # logger.info('OCR 引擎:\t%s', ocr.engine.info)
        logger.info('截圖路徑:\t%s', config.SCREEN_SHOOT_SAVE_PATH)

        if config.enable_baidu_api:
            logger.info('%s',
                        """百度API配置信息:
        APP_ID\t{app_id}
        API_KEY\t{api_key}
        SECRET_KEY\t{secret_key}
                        """.format(
                            app_id=config.APP_ID, api_key=config.API_KEY, secret_key=config.SECRET_KEY
                        )
                        )

    def __del(self):
        self.adb.run_device_cmd("am force-stop {}".format(config.ArkNights_PACKAGE_NAME))

    def destroy(self):
        self.__del()

    def check_game_active(self):  # 啟動游戲 需要手動調用
        logger.debug("helper.check_game_active")
        current = self.adb.run_device_cmd('dumpsys window windows | grep mCurrentFocus').decode(errors='ignore')
        logger.debug("正在嘗試啟動游戲")
        logger.debug(current)
        if config.ArkNights_PACKAGE_NAME in current:
            logger.debug("游戲已啟動")
        else:
            self.adb.run_device_cmd(
                "am start -n {}/{}".format(config.ArkNights_PACKAGE_NAME, config.ArkNights_ACTIVITY_NAME))
            logger.debug("成功啟動游戲")

    def __wait(self, n=10,  # 等待時間中值
               MANLIKE_FLAG=True, allow_skip=False):  # 是否在此基礎上設偏移量
        if MANLIKE_FLAG:
            m = uniform(0, 0.3)
            n = uniform(n - m * 0.5 * n, n + m * n)
        self.frontend.delay(n, allow_skip)

    def mouse_click(self,  # 點擊一個按鈕
                    XY):  # 待點擊的按鈕的左上和右下坐標
        assert (self.viewport == (1280, 720))
        logger.debug("helper.mouse_click")
        xx = randint(XY[0][0], XY[1][0])
        yy = randint(XY[0][1], XY[1][1])
        logger.info("接收到點擊坐標並傳遞xx:{}和yy:{}".format(xx, yy))
        self.adb.touch_tap((xx, yy))
        self.__wait(TINY_WAIT, MANLIKE_FLAG=True)

    def tap_rect(self, rc):
        hwidth = (rc[2] - rc[0]) / 2
        hheight = (rc[3] - rc[1]) / 2
        midx = rc[0] + hwidth
        midy = rc[1] + hheight
        xdiff = max(-1, min(1, gauss(0, 0.2)))
        ydiff = max(-1, min(1, gauss(0, 0.2)))
        tapx = int(midx + xdiff * hwidth)
        tapy = int(midy + ydiff * hheight)
        self.adb.touch_tap((tapx, tapy))
        self.__wait(TINY_WAIT, MANLIKE_FLAG=True)

    def tap_quadrilateral(self, pts):
        pts = np.asarray(pts)
        acdiff = max(0, min(2, gauss(1, 0.2)))
        bddiff = max(0, min(2, gauss(1, 0.2)))
        halfac = (pts[2] - pts[0]) / 2
        m = pts[0] + halfac * acdiff
        pt2 = pts[1] if bddiff > 1 else pts[3]
        halfvec = (pt2 - m) / 2
        finalpt = m + halfvec * bddiff
        self.adb.touch_tap(tuple(int(x) for x in finalpt))
        self.__wait(TINY_WAIT, MANLIKE_FLAG=True)

    def wait_for_still_image(self, threshold=16, crop=None, timeout=60, raise_for_timeout=True, check_delay=1):
        if crop is None:
            shooter = lambda: self.adb.screenshot(False)
        else:
            shooter = lambda: self.adb.screenshot(False).crop(crop)
        screenshot = shooter()
        t0 = time.monotonic()
        ts = t0 + timeout
        n = 0
        minerr = 65025
        message_shown = False
        while (t1 := time.monotonic()) < ts:
            if check_delay > 0:
                self.__wait(check_delay, False, True)
            screenshot2 = shooter()
            mse = imgreco.imgops.compare_mse(screenshot, screenshot2)
            if mse <= threshold:
                return screenshot2
            screenshot = screenshot2
            if mse < minerr:
                minerr = mse
            if not message_shown and t1-t0 > 10:
                logger.info("等待畫面靜止")
        if raise_for_timeout:
            raise RuntimeError("%d 秒內畫面未靜止，最小誤差=%d，閾值=%d" % (timeout, minerr, threshold))
        return None

    def module_login(self):
        logger.debug("helper.module_login")
        logger.info("發送坐標LOGIN_QUICK_LOGIN: {}".format(CLICK_LOCATION['LOGIN_QUICK_LOGIN']))
        self.mouse_click(CLICK_LOCATION['LOGIN_QUICK_LOGIN'])
        self.__wait(BIG_WAIT)
        logger.info("發送坐標LOGIN_START_WAKEUP: {}".format(CLICK_LOCATION['LOGIN_START_WAKEUP']))
        self.mouse_click(CLICK_LOCATION['LOGIN_START_WAKEUP'])
        self.__wait(BIG_WAIT)

    def module_battle_slim(self,
                           c_id=None,  # 待戰鬥的關卡編號
                           set_count=1000,  # 戰鬥次數
                           check_ai=True,  # 是否檢查代理指揮
                           **kwargs):  # 擴展參數:
        '''
        :param sub 是否為子程式 (是否為module_battle所調用)
        :param auto_close 是否自動關閉, 默認為 false
        :param self_fix 是否嘗試自動修復, 默認為 false
        :param MAX_TIME 最大檢查輪數, 默認在 config 中設置,
            每隔一段時間進行一輪檢查判斷作戰是否結束
            建議自定義該數值以便在出現一定失誤,
            超出最大判斷次數後有一定的自我修復能力
        :return:
            True 完成指定次數的作戰
            False 理智不足, 退出作戰
        '''
        logger.debug("helper.module_battle_slim")
        sub = kwargs["sub"] \
            if "sub" in kwargs else False
        auto_close = kwargs["auto_close"] \
            if "auto_close" in kwargs else False
        if set_count == 0:
            return c_id, 0
        self.operation_time = []
        count = 0
        remain = 0
        try:
            for _ in range(set_count):
                # logger.info("開始第 %d 次戰鬥", count + 1)
                self.operation_once_statemachine(c_id, )
                count += 1
                logger.info("第 %d 次作戰完成", count)
                self.frontend.notify('completed-count', count)
                if count != set_count:
                    # 2019.10.06 更新邏輯後，提前點擊後等待時間包括企鵝物流
                    if config.reporter:
                        self.__wait(SMALL_WAIT, MANLIKE_FLAG=True)
                    else:
                        self.__wait(BIG_WAIT, MANLIKE_FLAG=True)
        except StopIteration:
            # count: succeeded count
            logger.error('未能進行第 %d 次作戰', count + 1)
            remain = set_count - count
            if remain > 1:
                logger.error('已忽略餘下的 %d 次戰鬥', remain - 1)

        return c_id, remain


    def can_perform_refill(self):
        if not self.use_refill:
            return False
        if self.max_refill_count is not None:
            return self.refill_count < self.max_refill_count
        else:
            return True

    @dataclass
    class operation_once_state:
        state: Callable = None
        stop: bool = False
        operation_start: float = 0
        first_wait: bool = True
        mistaken_delegation: bool = False
        prepare_reco: dict = None

    def operation_once_statemachine(self, c_id):
        import imgreco.before_operation
        import imgreco.end_operation

        smobj = ArknightsHelper.operation_once_state()
        def on_prepare(smobj):
            count_times = 0
            while True:
                screenshot = self.adb.screenshot()
                recoresult = imgreco.before_operation.recognize(screenshot)
                if recoresult is not None:
                    logger.debug('當前畫面關卡：%s', recoresult['operation'])
                    if c_id is not None:
                        # 如果傳入了關卡 ID，檢查識別結果
                        if recoresult['operation'] != c_id:
                            count_times += 1
                            self.__wait(1, False)
                            if count_times <= 20:
                                logger.warning('不在關卡界面')
                                self.__wait(TINY_WAIT, False)
                                continue
                            else:
                                logger.error('{}次檢測後都不再關卡界面，退出進程'.format(count_times))
                                raise StopIteration()
                    break
                else:
                    count_times += 1
                    self.__wait(1, False)
                    if count_times <= 20:
                        logger.warning('不在關卡界面')
                        self.__wait(TINY_WAIT, False)
                        continue
                    else:
                        logger.error('{}次檢測後都不再關卡界面，退出進程'.format(count_times))
                        raise StopIteration()

            self.CURRENT_STRENGTH = int(recoresult['AP'].split('/')[0])
            ap_text = '理智' if recoresult['consume_ap'] else '門票'
            logger.info('當前%s %d, 關卡消耗 %d', ap_text, self.CURRENT_STRENGTH, recoresult['consume'])
            if self.CURRENT_STRENGTH < int(recoresult['consume']):
                logger.error(ap_text + '不足 無法繼續')
                if recoresult['consume_ap'] and self.can_perform_refill():
                    logger.info('嘗試回復理智')
                    self.tap_rect(recoresult['start_button'])
                    self.__wait(SMALL_WAIT)
                    screenshot = self.adb.screenshot()
                    refill_type = imgreco.before_operation.check_ap_refill_type(screenshot)
                    confirm_refill = False
                    if refill_type == 'item' and self.refill_with_item:
                        logger.info('使用道具回復理智')
                        confirm_refill = True
                    if refill_type == 'originium' and self.refill_with_originium:
                        logger.info('碎石回復理智')
                        confirm_refill = True
                    # FIXME: 道具回復量不足時也會嘗試使用
                    if confirm_refill:
                        self.tap_rect(imgreco.before_operation.get_ap_refill_confirm_rect(self.viewport))
                        self.refill_count += 1
                        self.__wait(MEDIUM_WAIT)
                        return  # to on_prepare state
                    logger.error('未能回復理智')
                    self.tap_rect(imgreco.before_operation.get_ap_refill_cancel_rect(self.viewport))
                raise StopIteration()

            if not recoresult['delegated']:
                logger.info('設置代理指揮')
                self.tap_rect(recoresult['delegate_button'])
                return  # to on_prepare state

            logger.info("理智充足 開始行動")
            self.tap_rect(recoresult['start_button'])
            smobj.prepare_reco = recoresult
            smobj.state = on_troop

        def on_troop(smobj):
            count_times = 0
            while True:
                self.__wait(TINY_WAIT, False)
                screenshot = self.adb.screenshot()
                recoresult = imgreco.before_operation.check_confirm_troop_rect(screenshot)
                if recoresult:
                    logger.info('確認編隊')
                    break
                else:
                    count_times += 1
                    if count_times <= 20:
                        logger.warning('等待確認編隊')
                        continue
                    else:
                        logger.error('{} 次檢測後不再確認編隊界面'.format(count_times))
                        raise StopIteration()
            self.tap_rect(imgreco.before_operation.get_confirm_troop_rect(self.viewport))
            smobj.operation_start = monotonic()
            smobj.state = on_operation

        def on_operation(smobj):
            if smobj.first_wait:
                if len(self.operation_time) == 0:
                    wait_time = BATTLE_NONE_DETECT_TIME
                else:
                    wait_time = sum(self.operation_time) / len(self.operation_time) - 7
                logger.info('等待 %d s' % wait_time)
                self.__wait(wait_time, MANLIKE_FLAG=False, allow_skip=True)
                smobj.first_wait = False
            t = monotonic() - smobj.operation_start

            logger.info('已進行 %.1f s，判斷是否結束', t)
            screenshot = self.adb.screenshot()
            if imgreco.end_operation.check_level_up_popup(screenshot):
                logger.info("等級提升")
                self.operation_time.append(t)
                smobj.state = on_level_up_popup
                return

            end_flag = imgreco.end_operation.check_end_operation(smobj.prepare_reco['style'], not smobj.prepare_reco['no_friendship'], screenshot)
            if not end_flag and t > 300:
                if imgreco.end_operation.check_end_operation2(screenshot):
                    self.tap_rect(imgreco.end_operation.get_end2_rect(screenshot))
                    screenshot = self.adb.screenshot()
                    end_flag = imgreco.end_operation.check_end_operation_main(screenshot)
            if end_flag:
                logger.info('戰鬥結束')
                self.operation_time.append(t)
                crop = imgreco.end_operation.get_still_check_rect(self.viewport)
                if self.wait_for_still_image(crop=crop, timeout=15, raise_for_timeout=True):
                    smobj.state = on_end_operation
                return
            dlgtype, ocrresult = imgreco.common.recognize_dialog(screenshot)
            if dlgtype is not None:
                if dlgtype == 'yesno' and '代理指揮' in ocrresult:
                    logger.warning('代理指揮出現失誤')
                    self.frontend.alert('代理指揮', '代理指揮出現失誤', 'warn')
                    smobj.mistaken_delegation = True
                    if config.get('behavior/mistaken_delegation/settle', False):
                        logger.info('以 2 星結算關卡')
                        self.tap_rect(imgreco.common.get_dialog_right_button_rect(screenshot))
                        self.__wait(2)
                        smobj.stop = True
                        return
                    else:
                        logger.info('放棄關卡')
                        self.tap_rect(imgreco.common.get_dialog_left_button_rect(screenshot))
                        # 關閉失敗提示
                        self.wait_for_still_image()
                        self.tap_rect(imgreco.common.get_reward_popup_dismiss_rect(screenshot))
                        # FIXME: 理智返還
                        self.__wait(1)
                        smobj.stop = True
                        return
                elif dlgtype == 'yesno' and '將會恢復' in ocrresult:
                    logger.info('發現放棄行動提示，關閉')
                    self.tap_rect(imgreco.common.get_dialog_left_button_rect(screenshot))
                else:
                    logger.error('未處理的對話框：[%s] %s', dlgtype, ocrresult)
                    raise RuntimeError('unhandled dialog')

            logger.info('戰鬥未結束')
            self.__wait(BATTLE_FINISH_DETECT, allow_skip=True)

        def on_level_up_popup(smobj):
            self.__wait(SMALL_WAIT, MANLIKE_FLAG=True)
            logger.info('關閉升級提示')
            self.tap_rect(imgreco.end_operation.get_dismiss_level_up_popup_rect(self.viewport))
            self.wait_for_still_image()
            smobj.state = on_end_operation

        def on_end_operation(smobj):
            screenshot = self.adb.screenshot()
            logger.info('離開結算畫面')
            self.tap_rect(imgreco.end_operation.get_dismiss_end_operation_rect(self.viewport))
            reportresult = penguin_stats.reporter.ReportResult.NotReported
            try:
                # 掉落識別
                drops = imgreco.end_operation.recognize(smobj.prepare_reco['style'], screenshot)
                logger.debug('%s', repr(drops))
                logger.info('掉落識別結果：%s', format_recoresult(drops))
                log_total = len(self.loots)
                for _, group in drops['items']:
                    for name, qty in group:
                        if name is not None and qty is not None:
                            self.loots[name] = self.loots.get(name, 0) + qty
                self.frontend.notify("combat-result", drops)
                self.frontend.notify("loots", self.loots)
                if log_total:
                    self.log_total_loots()
                if self.use_penguin_report:
                    reportresult = self.penguin_reporter.report(drops)
                    if isinstance(reportresult, penguin_stats.reporter.ReportResult.Ok):
                        logger.debug('report hash = %s', reportresult.report_hash)
            except Exception as e:
                logger.error('', exc_info=True)
            if self.use_penguin_report and reportresult is penguin_stats.reporter.ReportResult.NotReported:
                filename = os.path.join(config.SCREEN_SHOOT_SAVE_PATH, '未上報掉落-%d.png' % time.time())
                with open(filename, 'wb') as f:
                    screenshot.save(f, format='PNG')
                logger.error('未上報掉落截圖已保存到 %s', filename)
            smobj.stop = True

        smobj.state = on_prepare
        smobj.stop = False
        smobj.operation_start = 0

        while not smobj.stop:
            oldstate = smobj.state
            smobj.state(smobj)
            if smobj.state != oldstate:
                logger.debug('state changed to %s', smobj.state.__name__)

        if smobj.mistaken_delegation and config.get('behavior/mistaken_delegation/skip', True):
            raise StopIteration()


    def back_to_main(self):  # 回到主頁
        logger.info("正在返回主頁")
        retry_count = 0
        max_retry = 3
        while True:
            screenshot = self.adb.screenshot()

            if imgreco.main.check_main(screenshot):
                break

            # 檢查是否有返回按鈕
            if imgreco.common.check_nav_button(screenshot):
                logger.info('發現返回按鈕，點擊返回')
                self.tap_rect(imgreco.common.get_nav_button_back_rect(self.viewport))
                self.__wait(SMALL_WAIT)
                # 點擊返回按鈕之後重新檢查
                continue

            if imgreco.common.check_get_item_popup(screenshot):
                logger.info('當前為獲得物資畫面，關閉')
                self.tap_rect(imgreco.common.get_reward_popup_dismiss_rect(self.viewport))
                self.__wait(SMALL_WAIT)
                continue

            # 檢查是否在設置畫面
            if imgreco.common.check_setting_scene(screenshot):
                logger.info("當前為設置/郵件畫面，返回")
                self.tap_rect(imgreco.common.get_setting_back_rect(self.viewport))
                self.__wait(SMALL_WAIT)
                continue

            # 檢測是否有關閉按鈕
            rect, confidence = imgreco.common.find_close_button(screenshot)
            if confidence > 0.9:
                logger.info("發現關閉按鈕")
                self.tap_rect(rect)
                self.__wait(SMALL_WAIT)
                continue

            dlgtype, ocr = imgreco.common.recognize_dialog(screenshot)
            if dlgtype == 'yesno':
                if '基建' in ocr or '停止招募' in ocr or '好友列表' in ocr:
                    self.tap_rect(imgreco.common.get_dialog_right_button_rect(screenshot))
                    self.__wait(3)
                    continue
                elif '招募幹員' in ocr or '加急' in ocr:
                    self.tap_rect(imgreco.common.get_dialog_left_button_rect(screenshot))
                    self.__wait(3)
                    continue
                else:
                    raise RuntimeError('未適配的對話框')
            elif dlgtype == 'ok':
                self.tap_rect(imgreco.common.get_dialog_ok_button_rect(screenshot))
                self.__wait(1)
                continue
            retry_count += 1
            if retry_count > max_retry:
                raise RuntimeError('未知畫面')
            logger.info('未知畫面，嘗試重新識別 {}/{} 次'.format(retry_count, max_retry))
            self.__wait(3)
        logger.info("已回到主頁")

    def module_battle(self,  # 完整的戰鬥模塊
                      c_id,  # 選擇的關卡
                      set_count=1000):  # 作戰次數
        logger.debug("helper.module_battle")
        c_id = c_id.upper()
        if stage_path.is_stage_supported_ocr(c_id):
            self.goto_stage_by_ocr(c_id)
        else:
            logger.error('不支持的關卡：%s', c_id)
            raise ValueError(c_id)
        return self.module_battle_slim(c_id,
                                set_count=set_count,
                                check_ai=True,
                                sub=True)

    def main_handler(self, task_list, clear_tasks=False, auto_close=True):
        if len(task_list) == 0:
            logger.fatal("任務清單為空!")

        for c_id, count in task_list:
            # if not stage_path.is_stage_supported(c_id):
            #     raise ValueError(c_id)
            logger.info("開始 %s", c_id)
            flag = self.module_battle(c_id, count)

        logger.info("任務清單執行完畢")

    def clear_task(self):
        logger.debug("helper.clear_task")
        logger.info("領取每日任務")
        self.back_to_main()
        screenshot = self.adb.screenshot()
        logger.info('進入任務界面')
        self.tap_quadrilateral(imgreco.main.get_task_corners(screenshot))
        self.__wait(SMALL_WAIT)
        screenshot = self.adb.screenshot()

        hasbeginner = imgreco.task.check_beginners_task(screenshot)
        if hasbeginner:
            logger.info('發現見習任務，切換到每日任務')
            self.tap_rect(imgreco.task.get_daily_task_rect(screenshot, hasbeginner))
            self.__wait(TINY_WAIT)
            screenshot = self.adb.screenshot()
        self.clear_task_worker()
        logger.info('切換到每周任務') #默認進入見習任務或每日任務，因此無需檢測，直接切換即可
        self.tap_rect(imgreco.task.get_weekly_task_rect(screenshot, hasbeginner))
        self.clear_task_worker()

    def clear_task_worker(self):
        screenshot = self.adb.screenshot()
        kickoff = True
        # while imgreco.task.check_collectable_reward(screenshot):
        #     logger.info('完成任務')
        #     self.tap_rect(imgreco.task.get_collect_reward_button_rect(self.viewport))
        #     self.__wait(SMALL_WAIT)
        #     while True:
        #         screenshot = self.adb.screenshot()
        #         if imgreco.common.check_get_item_popup(screenshot):
        #             logger.info('領取獎勵')
        #             self.tap_rect(imgreco.common.get_reward_popup_dismiss_rect(self.viewport))
        #             self.__wait(SMALL_WAIT)
        #         else:
        #             break
        #     screenshot = self.adb.screenshot()
        # logger.info("獎勵已領取完畢")
        while True:
            if imgreco.common.check_nav_button(screenshot) and not imgreco.task.check_collectable_reward(screenshot):
                logger.info("獎勵已領取完畢")
                break
            if kickoff:
                logger.info('開始領取獎勵')
                kickoff = False
            self.tap_rect(imgreco.task.get_collect_reward_button_rect(self.viewport))
            screenshot = self.adb.screenshot(cached=False)

    def recruit(self):
        import imgreco.recruit
        from . import recruit_calc
        logger.info('識別招募標簽')
        tags = imgreco.recruit.get_recruit_tags(self.adb.screenshot())
        logger.info('可選標簽：%s', ' '.join(tags))
        result = recruit_calc.calculate(tags)
        logger.debug('計算結果：%s', repr(result))
        return result, tags
        
    def recruit_and_tap(self):
        result, orderedTags = self.recruit()
        currentRank = 0
        bestTaglist = ""
        for tags, operators, rank in result[::-1]:
            taglist = ','.join(tags)
            if rank >= 1:
                if rank > currentRank:
                    bestTaglist = tags
                    currentRank = rank
            logger.info("%s:\t %s\n" % (taglist, ' '.join(op[0] for op in operators)))
        if (currentRank > 0):
            self.select_recruit_tags(bestTaglist, orderedTags)
        return result

    def select_recruit_tags(self, tags, orderedTags):
        screenshot = self.adb.screenshot()
        taplist = imgreco.recruit.tap_recruit_tags(screenshot, tags, orderedTags)
        for tap_location in taplist:
            self.tap_quadrilateral(tap_location)


    def find_and_tap(self, partition, target):
        lastpos = None
        while True:
            screenshot = self.adb.screenshot()
            recoresult = imgreco.map.recognize_map(screenshot, partition)
            if recoresult is None:
                # TODO: retry
                logger.error('未能定位關卡地圖')
                raise RuntimeError('recognition failed')
            if target in recoresult:
                pos = recoresult[target]
                logger.info('目標 %s 坐標: %s', target, pos)
                if lastpos is not None and tuple(pos) == tuple(lastpos):
                    logger.error('拖動後坐標未改變')
                    raise RuntimeError('拖動後坐標未改變')
                if 0 < pos[0] < self.viewport[0]:
                    logger.info('目標在可視區域內，點擊')
                    self.adb.touch_tap(pos, offsets=(5, 5))
                    self.__wait(3)
                    break
                else:
                    lastpos = pos
                    originX = self.viewport[0] // 2 + randint(-100, 100)
                    originY = self.viewport[1] // 2 + randint(-100, 100)
                    if pos[0] < 0:  # target in left of viewport
                        logger.info('目標在可視區域左側，向右拖動')
                        # swipe right
                        diff = -pos[0]
                        if abs(diff) < 100:
                            diff = 120
                        diff = min(diff, self.viewport[0] - originX)
                    elif pos[0] > self.viewport[0]:  # target in right of viewport
                        logger.info('目標在可視區域右側，向左拖動')
                        # swipe left
                        diff = self.viewport[0] - pos[0]
                        if abs(diff) < 100:
                            diff = -120
                        diff = max(diff, -originX)
                    self.adb.touch_swipe2((originX, originY), (diff * 0.7 * uniform(0.8, 1.2), 0), max(250, diff / 2))
                    self.__wait(5)
                    continue

            else:
                raise KeyError((target, partition))

    def find_and_tap_episode_by_ocr(self, target):
        import imgreco.stage_ocr
        from resources.imgreco.map_vectors import ep2region, region2ep
        target_region = ep2region.get(target)
        if target_region is None:
            logger.error(f'未能定位章節區域, target: {target}')
            raise RuntimeError('recognition failed')
        vw, vh = imgreco.util.get_vwvh(self.viewport)
        episode_tag_rect = tuple(map(int, (35.185*vh, 39.259*vh, 50.093*vh, 43.056*vh)))
        next_ep_region_rect = (5.833*vh, 69.167*vh, 11.944*vh, 74.815*vh)
        prev_ep_region_rect = (5.833*vh, 15.370*vh, 11.944*vh, 21.481*vh)
        current_ep_rect = (50*vw+20.778*vh, 29.000*vh, 50*vw+62.778*vh, 71.333*vh)
        episode_move = (400 * self.viewport[1] / 1080)

        while True:
            screenshot = self.adb.screenshot()
            current_episode_tag = screenshot.crop(episode_tag_rect)
            current_episode_str = imgreco.stage_ocr.do_img_ocr(current_episode_tag)
            logger.info(f'當前章節: {current_episode_str}')
            if not current_episode_str.startswith('EPISODE'):
                logger.error(f'章節識別失敗, current_episode_str: {current_episode_str}')
                raise RuntimeError('recognition failed')
            current_episode = int(current_episode_str[7:])
            current_region = ep2region.get(current_episode)
            if current_region is None:
                logger.error(f'未能定位章節區域, current_episode: {current_episode}')
                raise RuntimeError('recognition failed')
            if current_region == target_region:
                break
            if current_region > target_region:
                logger.info(f'前往上一章節區域')
                self.tap_rect(prev_ep_region_rect)
            else:
                logger.info(f'前往下一章節區域')
                self.tap_rect(next_ep_region_rect)
        while current_episode != target:
            move = min(abs(current_episode - target), 2) * episode_move * (1 if current_episode > target else -1)
            self.__swipe_screen(move, 10, self.viewport[0] // 4 * 3)
            screenshot = self.adb.screenshot()
            current_episode_tag = screenshot.crop(episode_tag_rect)
            current_episode_str = imgreco.stage_ocr.do_img_ocr(current_episode_tag)
            logger.info(f'當前章節: {current_episode_str}')
            current_episode = int(current_episode_str[7:])

        logger.info(f'進入章節: {current_episode_str}')
        self.tap_rect(current_ep_rect)
        self.tap_rect(current_ep_rect)

    def find_and_tap_stage_by_ocr(self, partition, target, partition_map=None):
        import imgreco.stage_ocr
        target = target.upper()
        if partition_map is None:
            from resources.imgreco.map_vectors import stage_maps_linear
            partition_map = stage_maps_linear[partition]
        target_index = partition_map.index(target)
        while True:
            screenshot = self.adb.screenshot()
            tags_map = imgreco.stage_ocr.recognize_all_screen_stage_tags(screenshot)
            if not tags_map:
                tags_map = imgreco.stage_ocr.recognize_all_screen_stage_tags(screenshot, allow_extra_icons=True)
                if not tags_map:
                    logger.error('未能定位關卡地圖')
                    raise RuntimeError('recognition failed')
            logger.debug('tags map: ' + repr(tags_map))
            pos = tags_map.get(target)
            if pos:
                logger.info('目標在可視區域內，點擊')
                self.adb.touch_tap(pos, offsets=(5, 5))
                self.__wait(1)
                return

            known_indices = [partition_map.index(x) for x in tags_map.keys() if x in partition_map]

            originX = self.viewport[0] // 2 + randint(-100, 100)
            originY = self.viewport[1] // 2 + randint(-100, 100)
            move = randint(self.viewport[0] // 4, self.viewport[0] // 3)

            if all(x > target_index for x in known_indices):
                logger.info('目標在可視區域左側，向右拖動')
            elif all(x < target_index for x in known_indices):
                move = -move
                logger.info('目標在可視區域右側，向左拖動')
            else:
                logger.error('未能定位關卡地圖')
                raise RuntimeError('recognition failed')
            self.adb.touch_swipe2((originX, originY), (move, max(250, move // 2)))
            self.__wait(1)

    def find_and_tap_daily(self, partition, target, *, recursion=0):
        screenshot = self.adb.screenshot()
        recoresult = imgreco.map.recognize_daily_menu(screenshot, partition)
        if target in recoresult:
            pos, conf = recoresult[target]
            logger.info('目標 %s 坐標=%s 差異=%f', target, pos, conf)
            offset = self.viewport[1] * 0.12  ## 24vh * 24vh range
            self.tap_rect((*(pos - offset), *(pos + offset)))
        else:
            if recursion == 0:
                originX = self.viewport[0] // 2 + randint(-100, 100)
                originY = self.viewport[1] // 2 + randint(-100, 100)
                if partition == 'material':
                    logger.info('目標可能在可視區域左側，向右拖動')
                    offset = self.viewport[0] * 0.2
                elif partition == 'soc':
                    logger.info('目標可能在可視區域右側，向左拖動')
                    offset = -self.viewport[0] * 0.2
                else:
                    logger.error('未知類別')
                    raise StopIteration()
                self.adb.touch_swipe2((originX, originY), (offset, 0), 400)
                self.__wait(2)
                self.find_and_tap_daily(partition, target, recursion=recursion+1)
            else:
                logger.error('未找到目標，是否未開放關卡？')

    def goto_stage_by_ocr(self, stage):
        path = stage_path.get_stage_path(stage)
        self.back_to_main()
        logger.info('進入作戰')
        self.tap_quadrilateral(imgreco.main.get_ballte_corners(self.adb.screenshot()))
        self.__wait(TINY_WAIT)
        if path[0] == 'main':
            vw, vh = imgreco.util.get_vwvh(self.viewport)
            self.tap_rect((14.316*vw, 89.815*vh, 28.462*vw, 99.815*vh))
            self.find_and_tap_episode_by_ocr(int(path[1][2:]))
            self.find_and_tap_stage_by_ocr(path[1], path[2])
        elif path[0] == 'material' or path[0] == 'soc':
            logger.info('選擇類別')
            self.tap_rect(imgreco.map.get_daily_menu_entry(self.viewport, path[0]))
            self.find_and_tap_daily(path[0], path[1])
            self.find_and_tap(path[1], path[2])
        else:
            raise NotImplementedError()

    def get_credit(self):
        logger.debug("helper.get_credit")
        logger.info("領取信賴")
        self.back_to_main()
        screenshot = self.adb.screenshot()
        logger.info('進入好友列表')
        self.tap_quadrilateral(imgreco.main.get_friend_corners(screenshot))
        self.__wait(SMALL_WAIT)
        self.tap_quadrilateral(imgreco.main.get_friend_list(screenshot))
        self.__wait(SMALL_WAIT)
        logger.info('訪問好友基建')
        self.tap_quadrilateral(imgreco.main.get_friend_build(screenshot))
        self.__wait(MEDIUM_WAIT)
        building_count = 0
        while building_count < 10:
            screenshot = self.adb.screenshot()
            self.tap_quadrilateral(imgreco.main.get_next_friend_build(screenshot))
            self.__wait(MEDIUM_WAIT)
            building_count = building_count + 1
            logger.info('訪問第 %s 位好友', building_count)
        logger.info('信賴領取完畢')
        
    def daily_jobs(self):
        self.module_battle_slim(
            c_id=None,
            set_count=9999,
        )
        self.clear_task()
        self.get_credit()

    def get_building(self):
        logger.debug("helper.get_building")
        logger.info("清空基建")
        self.back_to_main()
        screenshot = self.adb.screenshot()
        logger.info('進入我的基建')
        self.tap_quadrilateral(imgreco.main.get_back_my_build(screenshot))
        self.__wait(MEDIUM_WAIT + 3)
        self.tap_quadrilateral(imgreco.main.get_my_build_task(screenshot))
        self.__wait(SMALL_WAIT)
        logger.info('收取製造產物')
        self.tap_quadrilateral(imgreco.main.get_my_build_task_clear(screenshot))
        self.__wait(SMALL_WAIT)
        logger.info('清理貿易訂單')
        self.tap_quadrilateral(imgreco.main.get_my_sell_task_1(screenshot))
        self.__wait(SMALL_WAIT + 1)
        self.tap_quadrilateral(imgreco.main.get_my_sell_tasklist(screenshot))
        self.__wait(SMALL_WAIT -1 )
        sell_count = 0
        while sell_count <= 6:
            screenshot = self.adb.screenshot()
            self.tap_quadrilateral(imgreco.main.get_my_sell_task_main(screenshot))
            self.__wait(TINY_WAIT)
            sell_count = sell_count + 1
        self.tap_quadrilateral(imgreco.main.get_my_sell_task_2(screenshot))
        self.__wait(SMALL_WAIT - 1)
        sell_count = 0
        while sell_count <= 6:
            screenshot = self.adb.screenshot()
            self.tap_quadrilateral(imgreco.main.get_my_sell_task_main(screenshot))
            self.__wait(TINY_WAIT)
            sell_count = sell_count + 1
        self.back_to_main()
        logger.info("基建領取完畢")

    def log_total_loots(self):
        logger.info('目前已獲得：%s', ', '.join('%sx%d' % tup for tup in self.loots.items()))

    def get_inventory_items(self, show_item_name=False):
        import imgreco.inventory
        all_items_map = {}
        if show_item_name:
            import penguin_stats.arkplanner
            all_items_map = penguin_stats.arkplanner.get_all_items_map()

        self.back_to_main()
        logger.info("進入倉庫")
        self.tap_rect(imgreco.inventory.get_inventory_rect(self.viewport))

        items_map = {}
        last_screen_items = None
        move = -randint(self.viewport[0] // 4, self.viewport[0] // 3)
        self.__swipe_screen(move)
        screenshot = self.adb.screenshot()
        while True:
            move = -randint(self.viewport[0] // 3.5, self.viewport[0] // 2.5)
            self.__swipe_screen(move)
            screen_items_map = imgreco.inventory.get_all_item_in_screen(screenshot)
            if last_screen_items == screen_items_map.keys():
                logger.info("讀取完畢")
                break
            if show_item_name:
                name_map = {all_items_map[k]['name']: screen_items_map[k] for k in screen_items_map.keys()}
                logger.info('name_map: %s' % name_map)
            else:
                logger.info('screen_items_map: %s' % screen_items_map)
            last_screen_items = screen_items_map.keys()
            items_map.update(screen_items_map)
            # break
            screenshot = self.adb.screenshot()
        if show_item_name:
            logger.info('items_map: %s' % {all_items_map[k]['name']: items_map[k] for k in items_map.keys()})
        return items_map

    def __swipe_screen(self, move, rand=100, origin_x=None, origin_y=None):
        origin_x = (origin_x or self.viewport[0] // 2) + randint(-rand, rand)
        origin_y = (origin_y or self.viewport[1] // 2) + randint(-rand, rand)
        self.adb.touch_swipe2((origin_x, origin_y), (move, max(250, move // 2)), randint(600, 900))

    def create_custom_record(self, record_name, roi_size=64, wait_seconds_after_touch=1,
                             description='', back_to_main=True, prefer_mode='match_template', threshold=0.7):
        record_dir = os.path.join(os.path.realpath(os.path.join(__file__, '../../')),
                                  os.path.join('custom_record/', record_name))
        if os.path.exists(record_dir):
            c = input('已存在同名的記錄, y 覆蓋, n 退出: ')
            if c.strip().lower() != 'y':
                return
            import shutil
            shutil.rmtree(record_dir)
        os.mkdir(record_dir)

        if back_to_main:
            self.back_to_main()

        EVENT_LINE_RE = re.compile(r"(\S+): (\S+) (\S+) (\S+)$")
        records = []
        record_data = {
            'screen_width': self.viewport[0],
            'screen_height': self.viewport[1],
            'description': description,
            'prefer_mode': prefer_mode,
            'back_to_main': back_to_main,
            'records': records
        }
        half_roi = roi_size // 2
        logger.info('滑動屏幕以退出錄制.')
        logger.info('開始錄制, 請點擊相關區域...')
        sock = self.adb.device_session_factory().shell_stream('getevent')
        f = sock.makefile('rb')
        while True:
            x = 0
            y = 0
            point_list = []
            touch_down = False
            screen = self.adb.screenshot()
            while True:
                line = f.readline().decode('utf-8', 'replace').strip()
                # print(line)
                match = EVENT_LINE_RE.match(line.strip())
                if match is not None:
                    dev, etype, ecode, data = match.groups()
                    if '/dev/input/event5' != dev:
                        continue
                    etype, ecode, data = int(etype, 16), int(ecode, 16), int(data, 16)
                    # print(dev, etype, ecode, data)

                    if (etype, ecode) == (1, 330):
                        touch_down = (data == 1)

                    if touch_down:
                        if 53 == ecode:
                            x = data
                        elif 54 == ecode:
                            y = data
                        elif (etype, ecode, data) == (0, 0, 0):
                            # print(f'point: ({x}, {y})')
                            point_list.append((x, y))
                    elif (etype, ecode, data) == (0, 0, 0):
                        break
            logger.debug(f'point_list: {point_list}')
            if len(point_list) == 1:
                point = point_list[0]
                x1 = max(0, point[0] - half_roi)
                x2 = min(self.viewport[0] - 1, point[0] + half_roi)
                y1 = max(0, point[1] - half_roi)
                y2 = min(self.viewport[1] - 1, point[1] + half_roi)
                roi = screen.crop((x1, y1, x2, y2))
                step = len(records)
                roi.save(os.path.join(record_dir, f'step{step}.png'))
                record = {'point': point, 'img': f'step{step}.png', 'type': 'tap',
                          'wait_seconds_after_touch': wait_seconds_after_touch,
                          'threshold': threshold, 'repeat': 1, 'raise_exception': True}
                logger.info(f'record: {record}')
                records.append(record)
                if wait_seconds_after_touch:
                    logger.info(f'請等待 {wait_seconds_after_touch}s...')
                    self.__wait(wait_seconds_after_touch)

                logger.info('繼續...')
            elif len(point_list) > 1:
                # 滑動時跳出循環
                c = input('是否退出錄制[Y/n]:')
                if c.strip().lower() != 'n':
                    logger.info('停止錄制...')
                    break
                else:
                    # todo 處理屏幕滑動
                    continue
        with open(os.path.join(record_dir, f'record.json'), 'w', encoding='utf-8') as f:
            json.dump(record_data, f, ensure_ascii=False, indent=4, sort_keys=True)

    def replay_custom_record(self, record_name, mode=None, back_to_main=None):
        from PIL import Image
        record_dir = os.path.join(os.path.realpath(os.path.join(__file__, '../../')),
                                  os.path.join('custom_record/', record_name))
        if not os.path.exists(record_dir):
            logger.error(f'未找到相應的記錄: {record_name}')
            raise RuntimeError(f'未找到相應的記錄: {record_name}')

        with open(os.path.join(record_dir, 'record.json'), 'r', encoding='utf-8') as f:
            record_data = json.load(f)
        logger.info(f'record description: {record_data.get("description")}')
        records = record_data['records']
        if mode is None:
            mode = record_data.get('prefer_mode', 'match_template')
        if mode not in ('match_template', 'point'):
            logger.error(f'不支持的模式: {mode}')
            raise RuntimeError(f'不支持的模式: {mode}')
        if back_to_main is None:
            back_to_main = record_data.get('back_to_main', True)
        if back_to_main:
            self.back_to_main()
        record_height = record_data['screen_height']
        ratio = record_height / self.viewport[1]
        x, y = 0, 0
        for record in records:
            if record['type'] == 'tap':
                repeat = record.get('repeat', 1)
                raise_exception = record.get('raise_exception', True)
                threshold = record.get('threshold', 0.7)
                for _ in range(repeat):
                    if mode == 'match_template':
                        screen = self.adb.screenshot()
                        gray_screen = screen.convert('L')
                        if ratio != 1:
                            gray_screen = gray_screen.resize((int(self.viewport[0] * ratio), record_height))
                        template = Image.open(os.path.join(record_dir, record['img'])).convert('L')
                        (x, y), r = imgreco.imgops.match_template(gray_screen, template)
                        x = x // ratio
                        y = y // ratio
                        logger.info(f'(x, y), r, record: {(x, y), r, record}')
                        if r < threshold:
                            if raise_exception:
                                logger.error('無法識別的圖像: ' + record['img'])
                                raise RuntimeError('無法識別的圖像: ' + record['img'])
                            break
                    elif mode == 'point':
                        # 這個模式屏幕尺寸寬高比必須與記錄中的保持一至
                        assert record_data['screen_width'] == int(self.viewport[0] * ratio)
                        x, y = record['point']
                        x = x // ratio
                        y = y // ratio
                    self.adb.touch_tap((x, y), offsets=(5, 5))
                    if record.get('wait_seconds_after_touch'):
                        self.__wait(record['wait_seconds_after_touch'])
