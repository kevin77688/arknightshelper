set masterfolder=%cd%
cd ".\ArknightsAutoHelper"
setlocal EnableDelayedExpansion
set PYTHON_EXECUTABLE=python
where /q py
if errorlevel 0 set PYTHON_EXECUTABLE=py
cd /d "%~dp0"
!PYTHON_EXECUTABLE! -m venv venv
call venv\Scripts\activate.bat
pip install -r requirements.txt
endlocal
pause
